FROM python:3.7

COPY pero-ocr.patch /
RUN set -e -o xtrace \
  ; apt-get -qy update \
  ; apt-get -qy --no-install-recommends install git \
                                                libgl1-mesa-glx \
                                                parallel \
                                                wget \
  ; git clone https://github.com/cneud/page-to-text.git \
  ; cd page-to-text \
  ; git checkout c4fcc9e \
  ; cd .. \
  ; git clone https://github.com/DCGM/pero-ocr.git \
  ; cd pero-ocr \
  ; git checkout 8908759 \
  ; git apply /pero-ocr.patch \
  ; wget -O- https://www.fit.vut.cz/~ihradis/pero/pero_eu_cz_print_newspapers_2020-10-09.tar.gz | tar xzv --strip-components=1 \
  ; pip install -U wheel pip \
  ; pip install protobuf==3.20.1 shapely==1.7.1 scikit-learn==1.0.2 \
  ; pip install . \
  ; pip install torchvision \
  ; python user_scripts/parse_folder.py -i /tmp -c config.ini 2>&1 | tee /dev/stderr | tail -1 | grep -q ^ZeroDivisionError

WORKDIR pero-ocr
